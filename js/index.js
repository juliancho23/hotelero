$(function () {
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover({
		trigger: 'hover'
	});
  	$('.carousel').carousel({
	  	interval: 2000
	})

	$('#exampleModal').on('show.bs.modal', function (e) {
	 	console.log('entre a mostrar modal');
	});
	$('#exampleModal').on('shown.bs.modal', function (e) {
	  	console.log('mostre modal');
	  	$('.btn-reserva').prop('disabled', true);
	  	$('.btn-reserva').removeClass('btn-primary');
	  	$('.btn-reserva').addClass('btn-danger');

	});
	$('#exampleModal').on('hide.bs.modal', function (e) {
		console.log('Entre a ocultar modal');
	});
	$('#exampleModal').on('hidden.bs.modal', function (e) {
		console.log('oculte modal');
		$('.btn-reserva').prop('disabled', false);
	  	$('.btn-reserva').removeClass('btn-danger');
	  	$('.btn-reserva').addClass('btn-primary');
	})
})